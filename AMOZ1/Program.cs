﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AMOZcore
{
    class Program
    {
        static void Main(string[] args)
        {
            int paramsNumber = 6;
            int k = 50;
            double minValue = -5.12;
            double maxValue = 5.12;
            //double minValue = -2.048;
            //double maxValue = 2.048;
            //double minValue = -10;
            //double maxValue = 10;
            double q = .06e-3;
            double ksi = 0.85;
            int size = 200;
            Func<double[], double> fitFunc1 = FitFn.HYPERSPHERE;
            Func<double[], double> fitFunc2 = FitFn.DEJONG;
            Func<double[], double> fitFunc3 = FitFn.ROZENBROK;
            IComparer<Item> solutionsComparator = new ComparatorMin(); //ComparatorMax
            int iterCount = 100;

            AntSolver asr = new AntSolver(paramsNumber, k, minValue, maxValue, q, ksi, size, fitFunc1, solutionsComparator, iterCount);
            asr.Solve();
            var solution = asr.Solution();
            var stringsol = solution.Select(s => s.ToString());
            Console.WriteLine("[\n{0}\n]", String.Join("\n", stringsol));
            Console.ReadKey();
        }
    }
}
