﻿using System;

namespace AMOZcore
{
    internal static class FitFn
    {
        public static double HYPERSPHERE(double[] args)
        {
            double sum = 0.0;

            foreach (double param in args)
            {
                sum += param * param;
            }

            return sum;
        }

        public static double DEJONG(double[] args)
        {
            double x1 = args[0];
            double x2 = args[1];
            return -100 * (Math.Pow(x2 - Math.Pow(x1, 2), 2) - Math.Pow(1 - x1, 2));
        }

        public static double ROZENBROK(double[] args)
        {
            double x1 = args[0];
            double x2 = args[1];
            return Math.Pow(1 - x1, 2) + 100 * Math.Pow(x2 - Math.Pow(x1, 2), 2);
        }
    }
}