﻿using System;

namespace AMOZcore
{
    internal class Ant
    {
        private double[] solution;

        public Ant(int size)
        {
            this.solution = new double[size];
        }

        public double[] GetSolution()
        {
            return solution;
        }

        public double[] Copy()
        {
            double[] newarr = new double[solution.Length];
            Array.Copy(solution, newarr, solution.Length);
            return newarr;
        }
    }
}