﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMOZcore
{
    internal class ComparatorMin : IComparer<Item>
    {
        public int Compare(Item ai1, Item ai2)
        {
            int result = 0;

            if (ai1.fitness > ai2.fitness)
            {
                result = -1;
            }
            else if (ai1.fitness < ai2.fitness)
            {
                result = 1;
            }

            return result;
        }
    }

    class ComparatorMax : IComparer<Item>
    {
        public int Compare(Item ai1, Item ai2)
        {
            int result = 0;

            if (ai1.fitness < ai2.fitness)
            {
                result = -1;
            }
            else if (ai1.fitness > ai2.fitness)
            {
                result = 1;
            }

            return result;
        }
    }
}
