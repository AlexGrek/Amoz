﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMOZcore
{
    internal class Arch
    {
        private List<Item> solutions;
        private double minValue;
        private double maxValue;
        private int solutionsNumber;

        Random rdm = new Random();

        public Arch(int paramsNumber, int solutionsNumber, double minValue, double maxValue)
        {
            solutions = new List<Item>();
            this.minValue = minValue;
            this.maxValue = maxValue;
            this.solutionsNumber = solutionsNumber;
            InitArchive(paramsNumber, 0);
        }

        public void addSolution(double[] newSolution)
        {
            solutions.Add(new Item(newSolution));
        }

        public int getSolutionsNumber()
        {
            return solutions.Count;
        }

        public int getSolutionSize()
        {
            return solutions[0].Solution.Length;
        }

        public Item ArchItems(int idx)
        {
            return solutions[idx];
        }

        public double[] CountOmegas(int k, double q)
        {
            double[] omegas = new double[solutions.Count];

            double kq = q * k;
            double kq2 = kq * kq;
            double k2q2_4 = kq2 * kq2 * 4;
            double kqSqrt2P = kq * Math.Sqrt(Math.PI * 2);

            for (int i = 1; i <= omegas.Length; i++)
            { // counts each omega from 1 to k
                double numerator = i - 1;
                omegas[i - 1] = 1.0 / (kqSqrt2P * Math.Pow(Math.E, numerator * numerator / k2q2_4));
            }

            return omegas;
        }

        public void UpdateSol()
        {
            for (int i = solutionsNumber; i < solutions.Count; i++)
            {
                solutions.RemoveAt(solutions.Count - 1);
            }
        }

        public void Sort(Func<double[], double> fitnessFunc, IComparer<Item> comparator)
        {
            foreach (Item solution in solutions)
            {
                solution.CountFitness(fitnessFunc);
            }

            solutions.Sort(comparator);
        }

    public override String ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (Item solution in solutions)
            {
                sb.AppendLine(solution.ToString());
            }

            return sb.ToString();
        }

        private void InitArchive(int paramsNumber, int startPos)
        {
            double shift = maxValue - minValue;

            for (int i = startPos; i < solutionsNumber; i++)
            {
                double[] solution = new double[paramsNumber];

                for (int j = 0; j < solution.Length; j++)
                {
                    solution[j] = minValue + rdm.NextDouble() * shift;
                }

                solutions.Add(new Item(solution));
            }
        }
    }
}