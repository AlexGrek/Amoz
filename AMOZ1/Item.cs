﻿using System;

namespace AMOZcore
{
    internal class Item
    {

        public double[] solution;
        public double omega;
        public double fitness;

        public Item(double[] solution)
        {
            this.solution = solution;
        }

        public double[] Solution { get { return solution; } }

        public void InitAt(int idx, double x)
        {
            solution[idx] = x;
        }

        public double Get(int i)
        {
            return solution[i];
        }

        public void UpdateSolution(double[] solution)
        {
            this.solution = solution;
        }

        public double GetOmega()
        {
            return omega;
        }

        public void SetOmega(double omega)
        {
            this.omega = omega;
        }

        public double GetFitness()
        {
            return fitness;
        }

        public void CountFitness(Func<double[], double> fitnessFunc)
        {
            fitness = Math.Abs(1 / (fitnessFunc(solution) + 1));
        }


        public override string ToString()
        {
            return String.Join(";", solution);
        }
    }
}