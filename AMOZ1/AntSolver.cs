﻿using System;
using System.Collections.Generic;

namespace AMOZcore
{
    internal class AntSolver
    {
        private Arch ar;
        private Func<double[], double> fitFn;
        private IComparer<Item> comp;
        private int iterationsNumber;
        private double[] omegas;
        private double ksi;
        private Ant[] colony;

        public AntSolver(int paramsNumber, int k, double minValue, double maxValue, double q, double ksi, int colonySize,
                   Func<double[], double> fitnessFunction, IComparer<Item> solutionsComparator, int iterationsNumber)
        {
            this.fitFn = fitnessFunction;
            this.comp = solutionsComparator;
            this.iterationsNumber = iterationsNumber;
            this.ksi = ksi;
            ar = new Arch(paramsNumber, k, minValue, maxValue);
            omegas = ar.CountOmegas(k, q);

            Init(colonySize, paramsNumber);
        }

        public void Solve()
        {
            ar.Sort(fitFn, comp);
            int bestIteration = 0;
            double[] best = ar.ArchItems(0).Solution;
            int paramsNumber = ar.getSolutionSize();

            for (int counter = 0; counter < iterationsNumber; counter++)
            {
                foreach (Ant ant in colony)
                {
                    int chosenGaussFunction = Tools.Roulette(omegas);
                    Item chosenItem = ar.ArchItems(chosenGaussFunction);

                    for (int i = 0; i < paramsNumber; i++)
                    {
                        double mue = chosenItem.Get(i);
                        double sigma = GetSigma(chosenGaussFunction, i);
                        double x = Tools.GaussianRandom(mue, sigma);
                        ant.GetSolution()[i] = x;
                    }

                }

                AddSolution();
                ar.Sort(fitFn, comp);
                ar.UpdateSol();

                if (best != ar.ArchItems(0).Solution)
                {
                    best = ar.ArchItems(0).Solution;
                    bestIteration = counter;
                }
            }

            Console.WriteLine("Last best solution iteration: " + bestIteration);
        }

        public double[] Solution()
        {
            return ar.ArchItems(0).Solution;
        }

        private void Init(int colonySize, int paramsNumber)
        {
            colony = new Ant[colonySize];

            for (int i = 0; i < colonySize; i++)
            {
                colony[i] = new Ant(paramsNumber);
            }
        }

        private double GetSigma(int chosenSolutionIdx, int paramIdx)
        {
            double sum = 0.0;

            for (int idx = 0; idx < ar.getSolutionsNumber(); idx++)
            {
                if (idx == chosenSolutionIdx)
                {
                    continue;
                }

                sum += Math.Abs(ar.ArchItems(chosenSolutionIdx).Get(paramIdx) - ar.ArchItems(idx).Get(paramIdx))
                        / (ar.getSolutionsNumber() - 1);
            }

            return ksi * sum;
        }

        private void AddSolution()
        {
            foreach (Ant ant in colony)
            {
                ar.addSolution(ant.Copy());
            }
        }
    }
}