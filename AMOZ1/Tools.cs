﻿using System;

namespace AMOZcore
{
    internal class Tools
    {
        public static Random Rand = new Random();

        public static int Roulette(double[] values)
        {
            double sum = 0.0;

            foreach (double value in values)
            {
                sum += value;
            }

            int idx = 0;
            double rouletteResult = Rand.NextDouble() * sum;
            sum = 0.0;

            while (idx < values.Length && sum < rouletteResult)
            {
                sum += values[idx++];
            }

            return idx - 1;
        }

        public static double GaussianRandom(double mue, double sigma)
        {
            double x1, x2, w, y;

            do
            {
                x1 = 2.0 * Rand.NextDouble() - 1.0;
                x2 = 2.0 * Rand.NextDouble() - 1.0;
                w = x1 * x1 + x2 * x2;
            } while (w >= 1.0);
            double llog = Math.Log(w);
            w = Math.Sqrt((-2.0 * llog) / w);
            y = x1 * w;

            return mue + sigma * y;
        }
    }
}