﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace AMOZ3
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] templates = { "T.csv", "P.csv", "W.csv" };
            Net network = new Net(templates);
            network.Recog("T_test.csv", 100);
            network.Recog("P_test.csv", 100);
            network.Recog("W_test.csv", 100);
            network.Recog("unkwn.csv", 100);

            Console.ReadKey();
        }
    }

    internal class Net
    {
        private int[][] weights;

        public Net(String[] templates)
        {
            int[][] inputTemplates = new int[templates.Length][];

            int idx = 0;
            foreach (String template in templates)
            {
                inputTemplates[idx++] = readInputFigure(template);
            }

            weights = countWeights(inputTemplates);
        }

        public void Recog(String filePath, int maxIterations)
        {
            int[] input = readInputFigure(filePath);
            int[] output = initNetwork(input);

            int[] newOutput = output;
            int iter = 0;
            int lastUpdateIteration = 0;

            Console.WriteLine("Before: \n" + writeOutputFigure(newOutput));

            do
            {
                if (!Enumerable.SequenceEqual(output, newOutput))
                {
                    lastUpdateIteration = iter;
                }

                output = newOutput;
                newOutput = asynchronousUpdate(output);
                //            newOutput = synchronousUpdate(output);
            } while (++iter < maxIterations);

            Console.WriteLine("Last update iteration: " + lastUpdateIteration);
            Console.WriteLine("After: \n" + writeOutputFigure(newOutput));
        }

        private int[] asynchronousUpdate(int[] output)
        {
            int[] newOutput = new int[output.Length];
            output.CopyTo(newOutput, 0);

            for (int i = 0; i < output.Length; i++)
            {
                int sum = 0;

                for (int j = 0; j < newOutput.Length; j++)
                {
                    sum += weights[i][j] * newOutput[j];
                }

                newOutput[i] = treshold(sum, output[i]);
            }

            return newOutput;
        }

        private int[] synchronousUpdate(int[] output)
        {
            int[] newOutput = new int[output.Length];

            for (int i = 0; i < output.Length; i++)
            {
                int sum = 0;

                for (int j = 0; j < output.Length; j++)
                {
                    sum += weights[i][j] * output[j];
                }

                newOutput[i] = treshold(sum, output[i]);
            }

            return newOutput;
        }

        private int[] readInputFigure(String filePath)
        {
            int[] result = null;

            String line;
            int pos = 0;
            result = new int[56];
            string[] lines = File.ReadAllLines(filePath);
            for (int row = 0; row < 8; row++)
            {
                line = lines[pos];
                pos++;
                String[] values = line.Split(';');

                for (int idx = 0; idx < 7; idx++)
                {
                    result[row * 7 + idx] = int.Parse(values[idx]);
                }
            }

            return result;
        }

        private String writeOutputFigure(int[] output)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    sb.Append(String.Format("{0,3}", output[i * 7 + j]));
                }

                sb.Append('\n');
            }

            return sb.ToString();
        }

        private int treshold(int sum, int output)
        {
            int result = output;

            if (sum < 0)
            {
                result = -1;
            }
            else if (sum > 0)
            {
                result = 1;
            }

            return result;
        }

        private int[][] countWeights(int[][] templates)
        {
            int[][] weights = new int[templates[0].Length][];
            weights.Fill2D(templates[0].Length, 0);

            for (int i = 0; i < weights.Length; i++)
            {
                for (int j = 0; j < weights[i].Length; j++)
                {
                    if (i == j)
                    {
                        continue;
                    }

                    int sum = 0;

                    foreach (int[] template in templates)
                    {
                        sum += template[i] * template[j];
                    }

                    weights[i][j] = sum;
                }
            }

            return weights;
        }

        private int[] initNetwork(int[] input)
        {
            int[] result = new int[input.Length];

            Array.Copy(input, 1, result, 1, result.Length - 1);

            return result;
        }
    }

    public static class ArrayExtensions
    {
        public static void Fill<T>(this T[] originalArray, T with)
        {
            for (int i = 0; i < originalArray.Length; i++)
            {
                originalArray[i] = with;
            }
        }

        public static void Fill2D<T>(this T[][] originalArray, int size, T with)
        {
            for (int i = 0; i < originalArray.Length; i++)
            {
                originalArray[i] = new T[size];
                originalArray[i].Fill(with);
            }
        }
    }
}
