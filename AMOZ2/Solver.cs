﻿using System;

namespace AMOZ2
{
    internal class Solver
    {
        private Graph graph;
        private int from;
        private int to;
        public Ant[] Colony;

        double Alpha;
        double Beta;

        private double R;
        private int maxi;

        public double A;

        private int bestPathLength = int.MaxValue;
        private int[] bestPath;

        public Solver(String graphFilePath, double initialPheromoneLevel, int moveFrom, int moveTo, int colonySize,
                   double alpha, double beta, double rho, int maxIterationsNumber, double a)
        {
            graph = new Graph(MyIO.ReadGraph(graphFilePath), initialPheromoneLevel);
            from = moveFrom;
            to = moveTo;
            Alpha = alpha;
            Beta = beta;
            R = rho;
            maxi = maxIterationsNumber;
            A = a;

            raiseColony(colonySize, moveFrom);
        }

        public void FindPath()
        {
            for (int iter = 0; iter < maxi; iter++)
            {
                FindShortestPaths();
                Evaporate();

                int bestPathLen = bestPathLength;
                Update();

                if (bestPathLen != bestPathLength)
                {
                    Console.WriteLine("Iteration: " + iter);
                    Console.WriteLine("Length: " + bestPathLength);
                    Console.WriteLine("Path: " + String.Join(",", bestPath));
                    Console.WriteLine();
                }
            }
        }

        private void FindShortestPaths()
        {
            foreach (var ant in Colony)
            {
                while (ant.LastLoc != to)
                {
                    int[] nearPoints = graph.CalculateNearPts(ant.LastLoc);
                    int[] visitedPoints = ant.PathAsArray();
                    int[] availablePoints = Tools.SubArr(nearPoints, visitedPoints);

                    if (availablePoints.Length == 0)
                    {
                        break;
                    }

                    int nexPoint = PathChoise(ant.LastLoc, availablePoints);
                    ant.moveToPoint(nexPoint);
                }
            }
        }

        private void Evaporate()
        {
            for (int i = 0; i < graph.Size(); i++)
            {
                for (int j = 0; j < graph.Size(); j++)
                {
                    graph.SetPherLevel(i, j, graph.GetPheromonesFromTo(i, j) * (1 - R));
                }
            }
        }

        private void Update()
        {
            int bestOnIteration = int.MaxValue;
            foreach (var ant in Colony)
            {
                int[] antPath = ant.PathAsArray();
                int pathLength = CalculatePathLen(antPath);
                if (pathLength < bestOnIteration)
                {
                    bestOnIteration = pathLength;
                }
            }

            int bestP = Tools.Rand.NextDouble() < 0.5 ? bestPathLength : bestOnIteration;

            double maxPheromone = 1 / ((1 - R) * bestP);
            double minPheromone = maxPheromone / A;

            foreach (Ant ant in Colony)
            {
                if (ant.LastLoc == to)
                {
                    int[] antPath = ant.PathAsArray();
                    int pathLength = CalculatePathLen(antPath);

                    if (pathLength < bestPathLength)
                    {
                        bestPathLength = pathLength;
                        bestPath = antPath;
                    }

                    double delta = 1.0 / bestP;

                    for (int idx = antPath.Length - 2; idx >= 0; idx--)
                    {
                        double newPheromone = graph.GetPheromonesFromTo(antPath[idx], antPath[idx + 1]) + delta;
                        if (newPheromone < minPheromone)
                        {
                            newPheromone = minPheromone;
                        }
                        else if (newPheromone > maxPheromone)
                        {
                            newPheromone = maxPheromone;
                        }

                        graph.SetPherLevel(antPath[idx], antPath[idx + 1], newPheromone);
                    }
                }

                ant.Reset();
            }
        }

        private void raiseColony(int colonySize, int startPoint)
        {
            Colony = new Ant[colonySize];

            for (int i = 0; i < colonySize; i++)
            {
                Colony[i] = new Ant(startPoint);
            }
        }

        private int PathChoise(int fromPoint, int[] possiblePoints)
        {
            double sum = 0.0;

            foreach (int nextPoint in possiblePoints)
            {
                double pheromone = graph.GetPheromonesFromTo(fromPoint, nextPoint);
                double nu = 1.0 / graph.getDistance(fromPoint, nextPoint);
                sum += Math.Pow(pheromone, Alpha) * Math.Pow(nu, Beta);
            }

            int idx = 0;
            double rouletteResult = Tools.Rand.NextDouble() * sum;
            sum = 0.0;

            while (idx < possiblePoints.Length && sum < rouletteResult)
            {
                double pheromone = graph.GetPheromonesFromTo(fromPoint, possiblePoints[idx]);
                double nu = 1.0 / graph.getDistance(fromPoint, possiblePoints[idx]);
                sum += Math.Pow(pheromone, Alpha) * Math.Pow(nu, Beta);
                idx++;
            }

            return possiblePoints[idx - 1];
        }

        private int CalculatePathLen(int[] pts)
        {
            int pathLength = 0;

            for (int idx = pts.Length - 2; idx >= 0; idx--)
            {
                pathLength += graph.getDistance(pts[idx], pts[idx + 1]);
            }

            return pathLength;
        }
    }
}