﻿using System;
using System.IO;

namespace AMOZ2
{
    internal static class MyIO
    {
        public static int[][] ReadGraph(string filePath)
        {
            Console.WriteLine("ReadGraph called");
            System.Threading.Thread.Sleep(400);

            int[][] result = null;

            int graphSize = 0;
            int position = 0;

            var lines = File.ReadAllLines(filePath);

            foreach (var line in lines)
            {
                position += 1;

                if (line.StartsWith("c"))
                {
                    continue;
                }

                if (line.StartsWith("p"))
                {
                    var graphSizes = line.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    var graphSizeStr = graphSizes[1];
                    graphSize = int.Parse(graphSizeStr);
                    break;
                }
            }

            if (graphSize == 0)
            {
                throw new ArgumentException();
            }

            result = new int[graphSize][];
            result.Fill2D(graphSize, 0);

            

            foreach (int[] resultRow in result)
            {
                var line = lines[position];
                position += 1;
                String[] values = line.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                for (int idx = 0; idx < graphSize; idx++)
                {
                    resultRow[idx] = int.Parse(values[idx + 1]);
                }
                // resultRow[values.Length] = values[values.Length - 1
                //Console.WriteLine($"{position} of {lines.Length} -> {resultRow.Length}, {graphSize} :: {values.Length} ({string.Join(", ", values)})");
            }

            return result;
        }
    }
}