﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AMOZ2
{
    static class Tools
    {
        public static Random Rand = new Random();


        public static int[] SubArr(int[] a1, int[] a2)
        {
            LinkedList<int> result = new LinkedList<int>();

            foreach (int point in a1)
            {
                int idx;

                for (idx = 0; idx < a2.Length; idx++)
                {
                    if (point == a2[idx])
                    {
                        break;
                    }
                }

                if (idx == a2.Length)
                {
                    result.AddLast(point);
                }
            }

            return result.ToArray();
        }
    }
}