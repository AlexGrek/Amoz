﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMOZ2
{
    class Program
    {
        public static double alpha = 0.2;
        public static double beta = 0.5;
        public static double rho = 0.1;

        static void Main(string[] args)
        {
            int sizeOfAntColony = 10;
            int maxIter = 100;
            double a = 100;
            string[] files = new string[] { "yuzSHP55.aco", "yuzSHP95.aco", "yuzSHP155.aco" };
            Solver pathfinder = new Solver(files[2], 0.1, 0, 94, sizeOfAntColony, alpha, beta, rho, maxIter, a);
            pathfinder.FindPath();

            Console.WriteLine("done");
            Console.ReadKey();
        }
    }
}
