﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AMOZ2
{
    internal class Ant
    {
        private int startPoint;
        private LinkedList<int> path;

        public Ant(int startPoint)
        {
            this.startPoint = startPoint;
            path = new LinkedList<int>();
            path.AddLast(startPoint);
        }

        public bool hasVisit(int point)
        {
            foreach (var visited in path)
            {
                if (point == visited)
                {
                    return true;
                }
            }

            return false;
        }

        public void moveToPoint(int nextPoint)
        {
            if (hasVisit(nextPoint))
            {
                throw new ArgumentException("visited " + nextPoint);
            }

            path.AddLast(nextPoint);
        }

        public int[] PathAsArray()
        {
            return path.ToArray();
        }

        public void Reset()
        {
            path.Clear();
            path.AddLast(startPoint);
        }

        public int LastLoc
        { get
            {
                return path.Last.Value;
            }
        }
    }
}