﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AMOZ2
{
    internal class Graph
    {
        private int[][] distances;
        private double[][] pheromones;

        public Graph(int[][] distances, double initialPheromonesLevel)
        {
            this.distances = distances;
            this.pheromones = new double[distances.Length][];

            for (var i = 0; i < pheromones.Length; i++)
            {
                pheromones[i] = new double[distances.Length];
            }

            foreach (double[] pheromoneRow in pheromones)
            {
                pheromoneRow.Fill(initialPheromonesLevel);
            }
        }

        public int Size()
        {
            return distances.Length;
        }

        public int getDistance(int fromPoint, int toPoint)
        {
            return distances[fromPoint][toPoint];
        }

        public double GetPheromonesFromTo(int fromPoint, int toPoint)
        {
            return pheromones[fromPoint][toPoint];
        }

        public void SetPherLevel(int row, int col, double value)
        {
            pheromones[row][col] = value;
        }

        public int[] CalculateNearPts(int point)
        {
            var nearPoints = new LinkedList<int>();
            int[] pointsRow = distances[point];

            for (int idx = 0; idx < pointsRow.Length; idx++)
            {
                if (pointsRow[idx] != 0)
                {
                    nearPoints.AddLast(idx);
                }
            }

            return nearPoints.ToArray();
        }
    }

    public static class ArrayExtensions
    {
        public static void Fill<T>(this T[] originalArray, T with)
        {
            for (int i = 0; i < originalArray.Length; i++)
            {
                originalArray[i] = with;
            }
        }

        public static void Fill2D<T>(this T[][] originalArray, int size, T with)
        {
            for (int i = 0; i < originalArray.Length; i++)
            {
                originalArray[i] = new T[size];
                originalArray[i].Fill(with);
            }
        }
    }
}